import me from './me.jpeg'
import mikasa from './mikasa.png'

export {
    me,
    mikasa
}