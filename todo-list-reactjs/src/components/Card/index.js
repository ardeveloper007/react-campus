import React from "react";
import { useDispatch } from "react-redux";
import SecondaryButton from "../Buttons/SecondaryButton";
import {
  deleteTaskAction,
  toggleTodoAction,
} from "../../redux/action/taskAction";
import PrimaryButton from "../Buttons/PrimaryButton";

export default function Card({ data, id }) {
  const dispatch = useDispatch();

  const btnCompleted = () => {
    dispatch(toggleTodoAction(id));
  };
  const btnDelete = () => {
    console.log(id);
    dispatch(deleteTaskAction(id));
  };

  return (
    <div className=" shadow-low px-3 py-6 flex  ">
      <div
        className={
          data.completed === false
            ? `no-underline w-full`
            : `line-through w-full`
        }
      >
        <p className="text-base font-bold">{data?.content.title}</p>
        <p className="text-sm">{data?.content.desc}</p>
        <div className="flex">
          <p className="mr-3">{data?.content.kota}</p>
          <p className="mr-3">{data?.content.posisi}</p>
          <p
            className={
              data?.content.category === "umum"
                ? `bg-green-500 rounded-lg px-2`
                : `bg-orange-500 rounded-lg px-2 `
            }
          >
            {data?.content.category}
          </p>
        </div>
        <div className="mt-5">
          <PrimaryButton to={"/edit" + id} text={"Edit"} />
          <PrimaryButton onClickFunction={() => btnDelete()} text={"Hapus"} />
        </div>
      </div>
      <div className="flex justify-center min-w-[150px] sm:ml-4 mt-4 ">
        <SecondaryButton
          onClickFunction={() => btnCompleted()}
          text={"Selesai"}
        />
      </div>
    </div>
  );
}
