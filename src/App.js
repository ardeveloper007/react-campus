import React from 'react'
import store from './redux/store'
import Router from './router'
import FlashMessage from 'react-native-flash-message'
import { NavigationContainer } from '@react-navigation/native'
import { LogBox } from 'react-native'
import { Provider, useSelector } from 'react-redux'
import { Loading, LoadingSmall } from './components'

const MainApp = () => {
  const stateGlobal = useSelector((state) => state)
  LogBox.ignoreLogs(['Setting a timer'])
  return (
    <>
      <NavigationContainer>
        <Router />
      </NavigationContainer>
      <FlashMessage position="top" />
      {stateGlobal.loading && <Loading />}  
       {stateGlobal.loadingSmall && <LoadingSmall />}
    </>
  )
}


const App = () => {
  return (
    <Provider store={store}>
      <MainApp />
    </Provider>
  )
}
export default App;


