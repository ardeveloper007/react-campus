import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { PrimaryButton } from "../../components";
import {
  LabelOptionsInput,
  LabelTextareainput,
  LabelTextInput,
} from "../../components/Input";
import { stringToObject } from "../../constants/stringToObject";
import useTask from "../../hooks/dependent/useTask";
import { ProductLayout } from "../../layout";
import { countryAction } from "../../redux/action/countryAction";
import { setLoading } from "../../redux/action/globalAction";
import { addTaskAction } from "../../redux/action/taskAction";

export default function Add() {
  const { addTask } = useTask();
  const dispatch = useDispatch();
  const { country } = useSelector((state) => state.countryReducer);
  const [title, setTitle] = useState("");
  const [desc, setDesc] = useState("");
  const [time] = useState(new Date());
  const [posisi, setPosisi] = useState("");
  const [nameCountry, setName_country] = useState("");
  const [category, setCategory] = useState("");

  // const word = "word";
  // console.log(word.replace(/['"]+/g, ""));

  useEffect(() => {
    dispatch(countryAction());
  });

  // const hadleAddTodo = () => {
  //   const data = {
  //     title: title,
  //     desc: desc,
  //     time: time,
  //     kota: nameCountry,
  //     category: category,
  //   };
  //   dispatch(setLoading(true));
  //   dispatch(addTaskAction(data));
  //   console.log(data);
  // };
  return (
    <>
      <form>
        <ProductLayout
          title={"Todo List"}
          button={
            <PrimaryButton text={"Tambah"} onClickFunction={() => addTask()} />
          }
        >
          <div className="mx-auto container sm:px-[100px] ">
            <LabelTextInput
              label={"Judul"}
              onChange={(e) => setTitle(e.target.value)}
              value={title}
            />
            <LabelTextareainput
              label={"Deskripsi"}
              onChange={(e) => setDesc(e.target.value)}
              value={desc}
            />

            {/* <div className={containerStyle}>
              <label className={labelStyle}>Kota</label>
              <br />
              <select
                onChange={(e) => setCategory(e.target.value)}
                className={inputStyle}
                placeholder="Kota Anda"
                value={category}
              >
                <option>Pilih katagori</option>
                {dataCategory.map((item) => (
                  <option value={item.name} key={item.id}>
                    {item.name}
                  </option>
                ))}
              </select>
            </div> */}

            {/* <LabelOptionsInput
              id={"city"}
              label={"Kota"}
              onChange={(e) => setName_country(e.target.value)}
              value={nameCountry || ""}
              values={country}
              valueOption={"nama_kota"}
              valueName={"nama_kota"}
            /> */}
            {/* <div className={containerStyle}>
              <label className={labelStyle}>Kota</label>
              <br />
              <select
                onChange={(e) => setName_country(e.target.value)}
                className={inputStyle}
                placeholder="Kota Anda"
                value={nameCountry}
              >
                <option>Pilih Kota</option>

                {country.map((item) => (
                  <option value={item.nama_kota} key={item.id}>
                    {item.nama_kota}
                  </option>
                ))}
              </select>
            </div> */}
          </div>
        </ProductLayout>
      </form>
    </>
  );
}
