import React from 'react';
import {
  StyleSheet, View
} from 'react-native';
import { Gap, List, ProfileDetail } from '../../../components';
import { colors } from '../../../utils';

const Profile_ = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Gap height={10} />
      
      <List
        name="Register"
        desc="if you haven't account"
        type="next"
        icon="edit-profile"
        onPress= {() => navigation.navigate('Register')}
      />
      <List
        name="Sign In"
        desc="You out from this account"
        type="next"
        icon="logout"
        onPress={()=> navigation.navigate('Login')}
      />
      <List
        name="Developer"
        desc="Detail about developer"
        type="next"
        icon="dev"
        onPress= {()=> navigation.navigate('Developer')}
      />
    </View>
  )
}
export default Profile_;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 40,
    backgroundColor: colors.white,
    flex: 1
  }
})
