import React from 'react'
import { useState } from 'react'
import {
  ScrollView,
  StyleSheet,
  View
} from 'react-native'
import {
  Button,
  Gap,
  Header,
  Input
} from '../../components'
import { colors, getData, showError, storeData, useForm } from '../../utils'
import { Fire } from '../../config'
import { useDispatch } from 'react-redux'


const Register = ({ navigation }) => {
  const dispatch = useDispatch()

  const [form, setForm] = useForm({
    name: '',
    email: '', 
    gender: 'Perempuan',
    job: '',
    password: '',
  })

  const [itemGender] = useState([
    {
      id: 1,
      label: 'Perempuan',
      value: 'perempuan'
    }, {
      id: 2,
      label: 'Laki-laki',
      value: 'laki-laki'
    }
  ])

  const onContinue = () => {
    console.log('hasil : ', form)
    dispatch({ type: 'SET_LOADING', value: true })
    Fire.auth()
      .createUserWithEmailAndPassword(form.email, form.password)
      .then((succes) => {
        setForm('reset')
        const data = {
          name: form.name,
          email: form.email,
          gender: form.gender,
          job: form.job,
          password: form.password,
          uid:succes.user.uid
        }
        Fire.database()
        .ref(`pengguna/${succes.user.uid}/`)
        .set(data)
        storeData('pengguna', data)
        getData('pengguna').then(res=>{
          console.log('isi store', res)
        })
        navigation.navigate('UploadPhoto', data)
        navigation.navigate('UploadPhoto')
        dispatch({ type: 'SET_LOADING', value: false })
        console.log('register success :', succes)
      }
      )
      .catch((err) => {
        showError(err.message)
        dispatch({ type: 'SET_LOADING', value: false })
      })
  }

  return (
    <View style={styles.page}>
      <Header onPress={() => navigation.goBack()} tittle={"Register"} />
      <View style={styles.content}>
        <ScrollView >
          <Input
            label="Name"
            value={form.name}
            onChangeText={(value) => setForm('name', value)}
          />

          <Gap height={24} />

          <Input
            label="Email"
            value={form.email}
            onChangeText={(value) => setForm('email', value)}
          />

          <Gap height={24} />

          <Input
            label="Gender"
            value={form.gender}
            onValueChange={(value) => setForm('gender', value)}
            select
            selectItem={itemGender}
          />

          <Gap height={24} />

          <Input
            label="Job"
            value={form.job}
            onChangeText={(value) => setForm('job', value)}
          />

          <Gap height={24} />

          <Input
            label="Password"
            value={form.password}
            onChangeText={(value) => setForm('password', value)}
            secureTextEntry
          />

          <Gap height={40} />
          <Button tittle="Continue" onPress={onContinue} />

          {/* <Button tittle="Continue" onPress={() => navigation.navigate('UploadPhoto')} /> */}
        </ScrollView>
      </View>
    </View>
  )
}
export default Register;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.white,
    flex: 1,
  },
  content: {
    padding: 40,
    paddingTop: 0
  }
})
