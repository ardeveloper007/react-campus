import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { IconNext, ILUpr } from '../../../assets'
import { colors, fonts } from '../../../utils'
import { Link } from '../../atoms'

const ListDetailRoom = ({ gambar, kategori, prodi, ruangan, deskripsi, onPress }) => {
    return (
        <View >
            <TouchableOpacity style={styles.container} onPress={onPress}>
                <View style={styles.line}>
                    <Image source={{ uri: gambar }} style={styles.img} />
                    <View style={styles.page} >
                        <Text style={styles.text}>{ruangan}</Text>
                        <Text style={styles.deskripsi} numberOfLines={2}  >
                            {deskripsi}
                        </Text>
                        <Link tittle='ReadMore' />
                        <View style={styles.icon} >
                            <IconNext />
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
            {/* <TouchableOpacity style={styles.container} onPress={onPress}>
                <View style={styles.line}>
                    <Image source={{ uri: gambar }} style={styles.img} />
                    <View style={styles.page} >
                        <Text style={styles.text}>{kategori}</Text>
                        <Text style={styles.text}>{prodi}</Text>
                        <Text style={styles.text}>{ruangan}</Text>
                        <Text style={styles.deskripsi} numberOfLines={3}  >
                            {deskripsi}
                        </Text>
                        <Link tittle='ReadMore'   />
                        <View style={styles.icon} >
                            <IconNext />
                        </View>
                    </View>
                </View>
            </TouchableOpacity> */}
        </View>
    )
}
export default ListDetailRoom;

const styles = StyleSheet.create({
    line: {
        borderBottomColor: colors.border1.third,
        borderBottomWidth: 1,
        flexDirection: 'row',
        flex: 1,
        height: 100,
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center',
    },
    page: {
        paddingHorizontal: 10,
        paddingBottom: 10,
        flex: 1,
    },
    icon: {
        position: 'absolute',
        alignContent: 'center',
        top: '50%',
        right: '1%',
        flex: 1,
    },
    deskripsi: {
        fontSize: 14,
        fontFamily: fonts.primary[500],
        color: colors.text.secondary
    },
    text: {
        maxWidth: '80%',
        fontSize: 14,
        fontFamily: fonts.primary[600],
        color: colors.text.primary
    },

    desc: {
        paddingVertical: 10,
        paddingRight: 50,
    },
    img: {
        width: 75,
        height: 90,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: colors.border1.third
    },
    container: {
        marginBottom: 0,
        marginTop: 1,
        paddingVertical: 10,
        paddingHorizontal: 10,
    },
})
