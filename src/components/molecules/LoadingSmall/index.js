import React from 'react'
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native'
import { colors, fonts } from '../../../utils'

const LoadingSmall = () => {
  return (
    <View style={styles.wrapper}>
      <ActivityIndicator color={colors.primary} />
    </View>
  )
}

export default LoadingSmall

const styles = StyleSheet.create({
  wrapper: {
    alignItems: 'center',
    backgroundColor: colors.white,
    // flex:1
  }
})
