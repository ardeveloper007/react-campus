import { combineReducers } from "redux";
import taskReducer from "./taskReducer";
import countryReducer from "./countryReducer";
import globalReducer from "./globalReducer";

const rootReducer = combineReducers({
  taskReducer,
  countryReducer,
  globalReducer,
});

export default rootReducer;
