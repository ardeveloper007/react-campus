import React from 'react'
import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { IconMapsActive } from '../../assets'
import {
    Button, Gap,
    Row
} from '../../components'
import { colors, fonts } from '../../utils'

const DetailRoomUmum= ({ navigation, route }) => {
    const dataRoom = route.params
    console.log('isi data', dataRoom)

    return (
        <>
            <View style={styles.container}>
                <Button type="icon-only" icon="back-dark" onPress={() => navigation.goBack()} />
                <Text style={styles.text}>{dataRoom.item.data.Ruangan}</Text>
                <TouchableOpacity onPress={() => navigation.navigate('DetailMapsUmum', dataRoom)} >
                    <IconMapsActive />
                </TouchableOpacity>
                <Gap width={24} />
            </View>
            <View style={styles.content}>
                <TouchableOpacity style={styles.photo} onPress={() => navigation.navigate('DetailPhoto', dataRoom.item.data.Gambar)}>
                    <Image source={{ uri: dataRoom.item.data.Gambar }} style={styles.img} />
                </TouchableOpacity>
                <ScrollView>
                    <Row tb1={'KATEGORI'} tb2={dataRoom.item.data.Kategori} />
                    <Row tb1={'PRODI'} tb2={dataRoom.item.data.Prodi} />
                    <View style={styles.des}>
                        <Text style={styles.title}>DESKRIPSI</Text>
                        <Text>{dataRoom.item.data.Deskripsi}</Text>
                    </View>
                    <Gap height={170} />
                </ScrollView>
            </View>
        </>
    )
}
export default DetailRoomUmum
const styles = StyleSheet.create({
    text: {
        textAlign: 'center',
        flex: 1,
        fontFamily: 'Nunito-SemiBold',
        color: colors.text.primary
      },
    container: {
        paddingHorizontal: 16,
        paddingVertical: 15,
        backgroundColor: colors.white,
        flexDirection: 'row',
        alignItems: 'center'
    },
    photo: {
        position: 'absolute',
        bottom: 0,
        left: 40,
        zIndex: 30,
    },
    img: {
        width: 100,
        height: 150,
        borderRadius: 5,
    },
    title: {
        fontFamily: fonts.primary[900],
        color: colors.text.primary,
        fontSize: 14,
    },
    des: {
        fontSize: 14,
        fontFamily: fonts.primary[500],
        color: colors.text.primary,
        marginBottom: 8,
        textAlign: 'justify',
    },
    content: {
        paddingHorizontal: 40,
        paddingTop: 0,
        flex: 1,
        backgroundColor: colors.white
    }
})
