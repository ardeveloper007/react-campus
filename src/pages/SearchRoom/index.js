import React, { useEffect, useState } from 'react';
import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View, TextInput } from 'react-native';
import { IconBackDark, IconSearch, ILHomePage } from '../../assets';
import {
    Gap,
    ListDetailRoom,
    MenuHome, ILRoom, LoadingSmall,
} from '../../components';
import { colors, showError, fonts } from '../../utils'
import { Fire } from '../../config'
import { useDispatch } from 'react-redux'

const SearchRoom = ({ navigation }) => {
    const dispatch = useDispatch()
    const [room, setRoom] = useState([])
    const [roomFilter, setRoomFilter] = useState([])

    const [border, setBorder] = useState(colors.border1.sec)
    const onFocusForm = () => {
        setBorder(colors.tertiary)
    }
    const onBlurForm = () => {
        setBorder(colors.border1.sec)
    }
    // useEffect(() => {
    dispatch({ type: 'SET_LOADING_SMALL', value: true })
    Fire.database()
        .ref('rooms/')
        .once('value')
        .then(res => {
            console.log('data val listroom: ', res.val())
            if (res.val()) {
                const oldData = res.val()
                const data = []
                Object.keys(oldData).map((key) => {
                    data.push({
                        id: key,
                        data: oldData[key]
                    })
                })
                setRoom(data)
            }
            dispatch({ type: 'SET_LOADING_SMALL', value: false })
        })
        .catch(err => {
            showError(err.message)
            dispatch({ type: 'SET_LOADING_SMALL', value: false })
        })
    console.log('data rooms di dlm useEffect: ', room)

    const search = (e) => {
        setRoomFilter(room.filter((i) => i.data.Ruangan.toUpperCase().includes(e.toUpperCase())))
    }
    return (
        <View style={{flex: 1, backgroundColor: colors.white}}>
            <View style={styles.header}>
                <TouchableOpacity style={styles.btn} onPress={() => navigation.replace('MainApp')}>
                    <IconBackDark />
                </TouchableOpacity>
                
                <View style={styles.container(border)}>
                    <IconSearch style={styles.img} />
                    <TextInput
                        style={styles.search}
                        placeholder='Search'
                        placeholderTextColor="#9FA5AF"
                        onFocus={onFocusForm}
                        onBlur={onBlurForm}
                        onChangeText={(e) => search(e.toUpperCase())}
                    />
                </View>
            </View>
            <ScrollView>
                <View style={styles.page}>

                    {roomFilter.map((item, index) => {
                        return (
                            <ListDetailRoom
                                key={index}
                                gambar={item.data.Gambar}
                                ruangan={item.data.Ruangan}
                                deskripsi={item.data.Deskripsi}
                                onPress={() => navigation.navigate('DetailRoomUmum', { item })}
                            />
                        )
                    })}
                </View>
            </ScrollView>
        </View>
    )
}
export default SearchRoom;

const styles = StyleSheet.create({
    btn: {
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
    },
    header: {
        // flex: 1, 
        backgroundColor: colors.white,
        flexDirection: 'row',
        paddingLeft: 12,
    },
    page: {
        marginVertical: 0,
        marginHorizontal: 24,
        backgroundColor: colors.white
    },
    img: {
        height: 24,
        width: 230,
    },
    container: border => ({
        flex: 1,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: colors.border1.sec,
        borderRadius: 10,
        marginVertical: 8,
        marginHorizontal: 24,
        paddingHorizontal: 8,
        alignItems: 'center',
        height: 40,
        borderColor: border,
        paddingHorizontal: 8,
    }),
    search: {
        fontFamily: fonts.primary[600],
        color: colors.text.primary,
        fontSize: 14,
        flex: 1,
    }
})
