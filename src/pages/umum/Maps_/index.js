import React, { useEffect, useState } from 'react'
import { Alert, PermissionsAndroid, StyleSheet, View } from 'react-native'
import Geolocation from 'react-native-geolocation-service'
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps'

const Maps_ = ({ navigation }) => {
  // const [latitude, setLatitude] = useState(0)
  // const [longitude, setLongitude] = useState(0)
  // const [latitudeDelta] = useState(0.0001)
  // const [longitudeDelta] = useState(0.0021)

  const [room, setRoom] = useState([])
  const [lang, setLang] = useState([])

  const [listRoom, setListRoom] = useState([
    {
      ruangan: 'TU',
      desc: 'TEKNIK INFORMATIKA',
      coordinate: {
        latitude: -2.2158532,
        longitude: 113.8985776
      },
    }, {
      title: "PERPUSTAKAAN",
      desc: 'TEKNIK INFORMATIKA',
      coordinate: {
        latitude: -2.2159007,
        longitude: 113.8985408
      },
    }, {
      title: "KEPALA JURUSAN",
      desc: 'TEKNIK INFORMATIKA',
      coordinate: {
        latitude: -2.2159268,
        longitude: 113.8985173
      },
    }, {
      title: "LAB APRO II",
      desc: 'TEKNIK INFORMATIKA',
      coordinate: {
        latitude: -2.2158717,
        longitude: 113.8985675
      },
    }
  ])

  useEffect(() => {
    permission();

    Alert.alert(
      "You must login your account",
      `Are you haven't account?`,
      [
        {
          text: 'NO', style: 'cancel',
          onPress: () => {
            navigation.goBack()
          }
        },
        {
          text: 'YES', onPress: () => {
            
            navigation.replace('Login')
          }
        },
      ]
    );

  }, [])
  


  const permission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: "Allow 'Location' to access your location",
          message:
            "This app need access to your location! ",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Don't Allow",
          buttonPositive: "Allow"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("Access your location");

        Geolocation.getCurrentPosition(
          (position) => {
            console.log(position);
          },
          (error) => {
            console.log(error.code, error.message);
          },
          {
            enableHighAccuracy: true,
            timeout: 3000,
            maximumAge: 10000,
            interval: 1000,
            accuracy: {
              android: 'high'
            },
            forceRequestLocation: true
          }
        );

      } else {
        console.log("Access location permission denied");
      }
    } catch (err) {
      console.warn(err);

    }

  }

  return (

    <View style={styles.content}>
      <View style={{ flex: 1 }}>
        <MapView
          provider={PROVIDER_GOOGLE}
          style={{ flex: 1 }}
          initialRegion={{
            latitude: -2.2158532,
            longitude: 113.8985776,
            latitudeDelta: 0.01,
            longitudeDelta: 0.01,
          }}
        >
          {
            listRoom.map((item, index) =>
              < Marker
                coordinate={item.coordinate}
                title={item.title}
                description={item.desc}
              />
            )
          }
        </MapView>
      </View>
    </View>

  )
}
export default Maps_;

const styles = StyleSheet.create({
  refresh: {
    width: 30,
    height: 25,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    zIndex: 10,
    position: 'absolute',
    top: 60,
    left: 4
  },
  content: {
    flex: 1,

  },
  searc: {
    zIndex: 3
  }
})
