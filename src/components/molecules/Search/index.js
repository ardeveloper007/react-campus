import React from 'react';
import { StyleSheet, TextInput, View, Text, TouchableOpacity } from 'react-native';
import { IconSearch, IconFilter } from '../../../assets';
import { colors, fonts } from '../../../utils';
import { useState } from 'react';
import { Gap } from '../../atoms';

const Search = ({onPress}) => {
    const [border, setBorder] = useState(colors.border1.sec)
    const onFocusForm = () => {
        setBorder(colors.tertiary)
    }
    const onBlurForm = () => {
        setBorder(colors.border1.sec)
    }
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <IconSearch style={styles.img} />
            <View style={styles.search}>
                <Text style={styles.text}>Masukkan Nama Ruangan</Text>
            </View>

            {/* <TextInput
                style={styles.search}
                placeholder='Search'
                placeholderTextColor="#9FA5AF"
                onFocus={onFocusForm}
                onBlur={onBlurForm}
            /> */}

        </TouchableOpacity>
    )
}
export default Search;

const styles = StyleSheet.create({
    text: {
        color: '#9FA5AF',
        fontSize: 14,
        fontFamily: fonts.primary[500],
        paddingLeft: 10
    },
    img: {
        height: 24,
        width: 230,
    },
    container: {
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: colors.border1.sec,
        borderRadius: 10,
        marginVertical: 8,
        marginHorizontal: 24,
        paddingHorizontal: 8,
        alignItems: 'center',
        height: 40,
        paddingHorizontal: 8,
    },
    search: {
        fontFamily: fonts.primary[500],
        color: colors.text.primary,
        fontSize: 14,
        flex: 1,
    }
})
