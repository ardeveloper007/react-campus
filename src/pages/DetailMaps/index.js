import React, { useEffect, useState } from 'react'
import { Image, PermissionsAndroid, StyleSheet, TouchableOpacity, View } from 'react-native'
import Geolocation from 'react-native-geolocation-service'
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps'
import { useDispatch } from 'react-redux'
import { IconMapsUser, IconRefresh } from '../../assets'


const DetailMaps = ({ navigation, route }) => {
  const dataRoom = route.params
  console.log('isi data', dataRoom)
  const [room, setRoom] = useState([])
  const [latitude, setLatitude] = useState(0)
  const [longitude, setLongitude] = useState(0)
  const dispatch = useDispatch()
  useEffect(() => {
    permission();
  }, [])

  const permission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: "Allow 'Location' to access your location",
          message:
            "This app need access to your location! ",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Don't Allow",
          buttonPositive: "Allow"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("Access your location");

        Geolocation.getCurrentPosition(
          (position) => {
            console.log(position);
            { setLongitude(position.coords.longitude) }
            { setLatitude(position.coords.latitude) }
          },
          (error) => {
            console.log(error.code, error.message);
          },
          {
            enableHighAccuracy: true,
            timeout: 3000,
            maximumAge: 10000,
            interval: 1000,
            accuracy: {
              android: 'high'
            },
            forceRequestLocation: true
          }
        );
        console.log('data rooms di dlm useEffect: ', room)
      } else {
        console.log("Access location permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }

  return (

    <View style={styles.content}>
      <TouchableOpacity style={styles.refresh} onPress={permission}>
        <IconRefresh />
      </TouchableOpacity>

      <View style={{ flex: 1 }}>
        <MapView
          provider={PROVIDER_GOOGLE}
          style={{ flex: 1 }}
          initialRegion={{
            latitude: -2.2158532,
            longitude: 113.8985776,
            latitudeDelta: 0.01,
            longitudeDelta: 0.01,
          }}
        >
          < Marker  
              coordinate={dataRoom.item.data.Coordinate}
              title={dataRoom.item.data.Ruangan}
              description={dataRoom.item.data.Kategori}
            />
          <Marker
            coordinate={{ latitude: latitude, longitude: longitude }}
            title="My Location"
          >
            <Image source={IconMapsUser} style={{ height: 30, width: 30 }} />
          </Marker>
        </MapView>
      </View>
    </View>

  )
}

export default DetailMaps;

const styles = StyleSheet.create({
  refresh: {
    width: 30,
    height: 25,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    zIndex: 10,
    position: 'absolute',
    top: 5,
    left: 4
  },
  content: {
    flex: 1,

  },
  searc: {
    zIndex: 3
  }
})
