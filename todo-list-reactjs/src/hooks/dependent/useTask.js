import { useState } from "react";
import { initialTask } from "../../utils/initial";

export default function useTask() {
  const [task, setTask] = useState(initialTask);
  const addTask = (async) => {
    console.log("hello");
  };

  return {
    addTask,
  };
}
