import React, { useEffect, useState } from 'react'
import { StyleSheet, ScrollView, Text, View } from 'react-native'
import { Header, ListDetailRoom, Search } from '../../components'
import { colors, fonts } from '../../utils'
import { useDispatch } from 'react-redux';
import { Fire } from '../../config'

const ListFasilitasUmum = ({ navigation }) => {
  const dispatch = useDispatch()
  const [room, setRoom] = useState([])
  
  useEffect(() => {
    dispatch({ type: 'SET_LOADING_SMALL', value: true })
    Fire.database()
      .ref('rooms/')
      .orderByChild("Kategori")
      .equalTo('Fasilitas Umum')
      .once('value')
      .then(res => {
        console.log('data val listroom: ', res.val())
        if (res.val()) {
          const oldData = res.val()
          const data = []
          Object.keys(oldData).map((key) => {
            data.push({
              id: key,
              data: oldData[key]
            })
          })
          setRoom(data)
        }
        dispatch({ type: 'SET_LOADING_SMALL', value: false })
      })
      .catch(err => {
        showError(err.message)
        dispatch({ type: 'SET_LOADING_SMALL', value: false })
      })
    console.log('data rooms di dlm useEffect: ', room)
  }, [])

  return (
    <View style={styles.page} >
      <Header tittle="Fasilitas Umum" onPress={() => navigation.goBack()} />
      <Search onPress={() => navigation.navigate('SearchRoom')} />
      <ScrollView>
        <View style={styles.containe}>
          {/* <ListDetailRoom /> */}
          {
            room.map((item, index) => {
              console.log('isi data Ruangan : ', item.data.Ruangan)
              return (
                <ListDetailRoom
                  key={index}
                  gambar={item.data.Gambar}
                  ruangan={item.data.Ruangan}
                  deskripsi={item.data.Deskripsi}
                  onPress={() => navigation.navigate('DetailRoomUmum', { item })}
                />
              )
            })}
        </View>
      </ScrollView>
    </View>
  )
}
export default ListFasilitasUmum;

const styles = StyleSheet.create({
  containe: {
    paddingHorizontal: 20
  },
  page: {
    flex: 1,
    backgroundColor: colors.white
  }
})
