import React from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'
import { IconHome, IconMapsActive, IconRoom, IconUser } from '../../../assets'
import { colors, fonts } from '../../../utils'
import { Gap } from '../../atoms'

const MenuHome = ({ title, onPress }) => {
    const IconMenu = () => {
        if (title === 'USER') {
            return <IconUser/>
        }if (title === 'ADMIN') {
            return <IconUser/>
        }
        if (title === 'ROOM') {
            return <IconRoom  />
        }
        if(title === 'Maps'){
            return <IconMapsActive />
        }

        return <IconHome />
    }
    return (
        
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <IconMenu />
            <Gap height={8} />
            <Text style={styles.text}>
                {title}
            </Text>
        </TouchableOpacity>
    )
}
export default MenuHome;

const styles = StyleSheet.create({
    container: {
        borderWidth: 1,
        width: 80,
        height: 70,
        alignItems: 'center',

        paddingVertical: 12,
        // borderWidth: 1,
        borderRadius: 10,
        borderColor: colors.border1.sec,
        // borderBottomWidth: 0,
        // shadowColor: '#000',
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.8,
        // shadowRadius: 2,
        // elevation: 1,
    },
    text: {
        fontFamily: fonts.normal,
        fontSize: 18,
    }
})
