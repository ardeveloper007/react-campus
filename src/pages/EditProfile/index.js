import React from 'react'
import {
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView
} from 'react-native'
import {
    ProfileDetail, Button,
    Search, Input, Gap, Header
} from '../../components'
import { ILHomePage, ILNullPhoto, me, mikasa } from '../../assets';
import { colors } from '../../utils';

const EditProfile = ({ navigation }) => {
    return (
        <>
            <Header tittle="Update Profile" onPress={() => navigation.goBack()} />
            <View style={styles.container}>
                <ScrollView>
                    <View style={styles.photo}>
                        <View style={styles.avatarWrapper}>
                            <Image style={styles.avatar} source={me } />
                        </View>
                    </View>
                    <View>
                        <Input
                            label="Name"
                            value="Diana"
                        />
                        <Gap height={24} />
                        <Input
                            label="Email"
                            value="Diana@gmail.com"
                            editable={false}
                            selectTextOnFocus={false}
                        />
                        <Gap height={24} />
                        <Input
                            label="Gender"
                            value="Perempuan"
                        />
                        <Gap height={24} />
                        <Input
                            label="Job"
                            value="Mahasiswa"
                        />
                        <Gap height={24} />
                        <Input
                            label="Password"
                            value="dmndff"
                            secureTextEntry
                        />
                    </View>
                    <Gap height={30} />
                    <Button tittle="Save Profile" />
                    <Gap height={64} />
                </ScrollView>
            </View>
        </>
    )
}
export default EditProfile;

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 40,
        flex: 1,
        backgroundColor: colors.white,
        flex: 1
    },
    photo: {
        marginVertical: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    avatar: {
        width: 110,
        height: 110,
        borderRadius: 110 / 2
    },
    avatarWrapper: {
        width: 130,
        height: 130,
        borderWidth: 1,
        borderColor: colors.border,
        borderRadius: 130 / 2,
        alignItems: 'center',
        justifyContent: 'center'
    },
})
