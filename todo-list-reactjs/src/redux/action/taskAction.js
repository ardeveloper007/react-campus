// import { useDispatch } from "react-redux";
import { setLoading } from "./globalAction";

export const addTaskAction = (content) => (dispatch) => {
  const id = Math.floor(Math.random() * 101);
  dispatch({
    type: "ADD_TASK",
    payload: id,
    content: content,
  });
  dispatch(setLoading(false));
};
export const updateTaskAction = (no, content) => {
  console.log("no", no);
  console.log("content", content);
  return {
    type: "UPDATE_TASK",
    payload: { no },
    content: content,
  };
};
export const deleteTaskAction = (idDelete) => {
  console.log("action", idDelete);
  return {
    type: "DELETE_TASK",
    payload: { idDelete },
  };
};
export const toggleTodoAction = (id) => {
  return {
    type: "TOGGLE_TODO",
    payload: { id },
  };
};
