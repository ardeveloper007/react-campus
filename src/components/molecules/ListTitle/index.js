import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View,  } from 'react-native'
import { fonts, colors } from '../../../utils'

const ListTile = ({title, onPress}) => {
    return (
        <View style={styles.card}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 4 }}>
                <View >
                    <Text style={styles.title}>{title}</Text>
                </View>
                <TouchableOpacity onPress={onPress}>
                    <Text style={styles.desc}>See All</Text>
                </TouchableOpacity>
            </View>
        </View>

    )
}
export default ListTile;

const styles = StyleSheet.create({
    card: {
        marginBottom: 4
    },
    title: {
        fontFamily: fonts.primary[600],
        fontSize: 16,
        color: colors.text.primary
    },
    desc: {
        color: colors.text.secondary,
        fontFamily: fonts.primary[600],
        fontSize: 16,
    },
})
