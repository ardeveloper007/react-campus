import React from 'react'
import { Image, StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { IconNext, ILRoom,ILNullPhoto, IconClose } from '../../../assets'
import { colors, fonts } from '../../../utils'
import { Link } from '../../atoms'

const AdminListDetailRoom = ({ gambar, kategori, prodi, ruangan, deskripsi, onPress, onPressDelete }) => {
  if(prodi){
    return(
      <View >
         <View style={styles.container}>
          <View style={styles.line}>
            <Image source={{ uri: gambar }} style={styles.img} />
            <View style={styles.page} >
              <Text style={styles.text}>{kategori}</Text>
              <Text style={styles.text}>{prodi}</Text>
              <Text style={styles.text}>{ruangan}</Text>
              <Text style={styles.deskripsi} numberOfLines={3}  >
                {deskripsi}
              </Text>
              <TouchableOpacity style={styles.close} onPress={onPressDelete} >
                <IconClose />
              </TouchableOpacity>
              <Link tittle='ReadMore' onPress={onPress} />
            </View>
          </View>
        </View> 
    </View>
    )
  }
  return (
    <View >
         <View style={styles.container}>
          <View style={styles.line}>
            <Image source={{ uri: gambar }} style={styles.img} />
            <View style={styles.page} >
              <Text style={styles.text}>{kategori}</Text>
              <Text style={styles.text}>{ruangan}</Text>
              <Text style={styles.deskripsi} numberOfLines={4}  >
                {deskripsi}
              </Text>
              <TouchableOpacity style={styles.close} onPress={onPressDelete} >
                <IconClose />
              </TouchableOpacity>
              <Link tittle='ReadMore' onPress={onPress} />
            </View>
          </View>
        </View> 
    </View>
  )
}
export default AdminListDetailRoom;

const styles = StyleSheet.create({
  close: {
    position: 'absolute',
    top: 5,
    right: 5
  },
  line: {
    borderBottomColor: colors.border1.third,
    borderBottomWidth: 1,
    flexDirection: 'row',
    flex: 1,
    height: 165,
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
  },
  page: {
    paddingHorizontal: 10,
    paddingBottom: 10,
    flex: 1,
  },
  icon: {
    position: 'absolute',
    alignContent: 'center',
    top: '50%',
    right: '1%',
    flex: 1,
  },
  deskripsi: {
    fontSize: 14,
    fontFamily: fonts.primary[500],
    color: colors.text.secondary
  },
  text: {
    maxWidth: '80%',
    fontSize: 16,
    fontFamily: fonts.primary[600],
    color: colors.text.primary
  },

  desc: {
    paddingVertical: 10,
    paddingRight: 50,
  },
  img: {
    width: 130,
    height: 157,
    borderRadius: 10,
  },
  container: {
    marginBottom: 0,
    marginTop: 8,

    paddingVertical: 10,
    paddingHorizontal: 10,
  },
})
