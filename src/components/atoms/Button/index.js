import React from 'react'
import { StyleSheet, Text, View, style, TouchableOpacity } from 'react-native'
import { colors, fonts } from '../../../utils';
import IconOnly from './IconOnly';

const Button = ({ type, tittle, onPress, icon }) => {

  if (type === 'icon-only') {
    return <IconOnly icon={icon} onPress={onPress} />
  }
  return (
    <TouchableOpacity style={styles.container(type)} onPress={onPress}>
      <Text style={styles.text(type)}>{tittle}</Text>
    </TouchableOpacity>
  )
}
export default Button;

const styles = StyleSheet.create({
  container: (type) => ({
    backgroundColor: type === colors.button.secondary.background ? 'white' : colors.button.primary.background,
    paddingVertical: 10,
    borderRadius: 10,
  }),
  text: (type) => ({
    fontSize: 18,
    fontWeight: '600',
    fontFamily: fonts.primary[500],
    textAlign: 'center',
    color: type === 'secondary' ? colors.button.secondary.text : colors.button.primary.text
  })
})
