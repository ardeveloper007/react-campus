import React from "react";
import { Link } from "react-router-dom";

export default function PrimaryButton({
  text,
  onClickFunction = () => {},
  additionalStyles = ``,
  to = "/",
}) {
  return (
    <Link
      to={to}
      className={`${additionalStyles} h-10 py-2 px-3 text-white bg-primary-05  rounded-xl`}
      onClick={onClickFunction}
    >
      <button>{text}</button>
    </Link>
  );
}
